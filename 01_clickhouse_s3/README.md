# Интеграция ClickHouse с S3 сервисом

## Сценарий
1. Подготовка S3 хранилища
2. Подключение к ClickHouse через ClickCat
3. Работа с S3 таблицами в ClickHouse

## Подготовка S3 хранилища
1. Открыть Minio UI через браузер (логин/пароль: `minioadmin`/`minioadmin`)
2. Создать Bucket `clickhouse` через меню `Buckets`
3. Настроить Bucket `clickhouse` через меню `Buckets`:
   * Установить атрибут `Access Policy` равным `public`
   * Добавить правило со значением `readwrite` в секции `Anonymous Access`
4. Загрузить CSV файл через меню `Object Browser` в Bucket `clickhouse`

## Подключение к ClickHouse через ClickCat
1. Открыть ClickCat UI через браузер
2. Создать подключение к ClickHouse (логин/пароль: `default`/`password`)
3. Создать базу данных `my_db` в ClickHouse
```clickhouse
create database if not exists
    my_db;
```

## Работа с S3 таблицами в ClickHouse

### Чтение данных из S3 в ClickHouse
1. Создать таблицу на движке S3 для чтения загруженного файла
```clickhouse
CREATE TABLE
    my_db.s3_samples(
        id Int,
        val Int,
        comment String)
ENGINE=S3('http://minio.minio.svc.cluster.local:9000/clickhouse/samples.csv', 'CSV')
```
2. Чтение данных
```clickhouse
select
    id,
    val,
    comment
from
    my_db.s3_samples
```

### Чтение данных из S3 в ClickHouse
1. Создать таблицу на движке S3
```clickhouse
CREATE TABLE
   my_db.s3_rw_samples(
     id Int,
     payload String)
ENGINE=S3('http://minio.minio.svc.cluster.local:9000/clickhouse/rw_samples.bin', 'Native')
```
2. Вставка данных
```clickhouse
insert into
   my_db.s3_rw_samples
settings
   s3_create_new_file_on_insert = 1
values
   (1, 'str1')
   (2, 'str2')
```
3. Чтение данных
```clickhouse
select
   id,
   payload
from
    my_db.s3_rw_samples
```
