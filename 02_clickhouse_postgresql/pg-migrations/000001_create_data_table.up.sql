CREATE TABLE IF NOT EXISTS
    data_table (
        id serial NOT NULL PRIMARY KEY,
        data Text);
