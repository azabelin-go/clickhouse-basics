# Интеграция ClickHouse с S3 сервисом

## Сценарий
1. Подготовка PostgreSQL таблиц
2. Подключение к ClickHouse через ClickCat
3. Работа с PostgreSQL таблицами в ClickHouse

## Подготовка PostgreSQL таблиц
1. Запустить перенаправление портов в Kubernetes к PostgreSQL:
```shell
kubectl -n postgres port-forward postgres-0 5432:5432
```
2. Создать таблицы через утилиту `go-migrate`:
```shell
migrate -path 02_clickhouse_postgresql/pg-migrations \
  -database "postgres://postgres:password@localhost:5432/postgres?sslmode=disable" \
  up
```

## Подключение к ClickHouse через ClickCat
1. Открыть ClickCat UI через браузер
2. Создать подключение к ClickHouse (логин/пароль: `default`/`password`)
3. Создать базу данных `my_db` в ClickHouse
```clickhouse
create database if not exists
    my_db;
```

## Работа с PostgreSQL таблицами в ClickHouse

### Подключение удалённой PostgreSQL базы данных в ClickHouse
1. Создать таблицу на движке PostgreSQL для чтения загруженного файла
```clickhouse
create table
   my_db.pg_data_table(
      data String)
engine = PostgreSQL(
    'postgres.postgres.svc.cluster.local:5432',
    'postgres',
    'data_table',
    'postgres',
    'password');
```
2. Чтение данных
```clickhouse
select
    id,
    data
from
    my_db.pg_data_table
```
3. Вставка данных
```clickhouse
insert into
    my_db.pg_data_table(
        data)
values
    ('clickhouse_data1');
```

### Подключение удалённой PostgreSQL базы данных в ClickHouse как словаря
1. Подключение удалённой PostgreSQL базы данных в ClickHouse как словаря
```clickhouse
create dictionary
    my_db.postgres_dict(
        id Int,
        name String)
primary key
    id
source(
    postgresql(
        host 'postgres.postgres.svc.cluster.local'
        port 5432
        user 'postgres'
        password 'password'
        db 'clickhouse'
        table 'dict_table'))
lifetime(
    min 300
    max 600)
layout(
    hashed());
```
2. Чтение данных
```clickhouse
select
    dictGetString(
        my_db.postgres_dict,
        'name',
        3);
```

### Репликация удалённой PostgreSQL базы данных в ClickHouse
1. Создать базу данных для репликации в ClickHouse
```clickhouse
SET allow_experimental_database_materialized_postgresql = 1;

create database
    pg_replication
engine = MaterializedPostgreSQL(
    'postgres.postgres.svc.cluster.local:5432',
    'postgres',
    'postgres', 'password');
```
2. Просмотр реплицируемых таблиц из PostgreSQL в ClickHouse
```clickhouse
--  список таблиц
show tables from
    pg_replication;

-- просмотр DDL запроса таблиц
show create table
    pg_replication.data_table;
```

3. Просмотр данных в реплицируемой таблице в ClickHouse
```clickhouse
select
    id,
    data
from
    pg_replication.data_table;
```

4. Запись данных в реплицируемую таблицу в PostgreSQL
```shell
kubectl -n postgres exec \
  -it pod/postgres-0 -- psql -c \
  "insert into data_table(data) values ('data_' || TO_CHAR(
        CURRENT_TIMESTAMP,
        'HH24_MI_SS'
    ));"
```

5. Просмотр данных в реплицируемой таблице в ClickHouse
```clickhouse
select
    id,
    data
from
    pg_replication.data_table;
```
