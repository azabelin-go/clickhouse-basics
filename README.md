# ClickHouse вводный курс
## Описание
Репозиторий с манифестами K8S для подготовки стенда и примерами скриптов для работы с ClickHouse 

## Минимальные требования
* Операционная система с установленной утилитой для развертывания кластера Kubernetes - `microk8s`/`minikube`
* Установленная и сконфигурированная утилита для работы с кластером Kubernetes - `kubectl`
* Доступ в терминал
* Средства разработки:
  * **Опционально:** Docker/Podman
  * **Опционально:** Golang
  * Python
  * Утилита `jq`
  * Утилита `make`
  * Утилита `go-migrate`
  * Утилита `curl`

## Инструкции

### Подготовка стенда
1. Запуск локального кластера Kubernetes через MicroK8S
```shell
# Запуск кластера
microk8s install
# Подключение плагинов
microk8s enable dns metrics-server hostpath-storage ingress
```
2. Получение файла `.kubeconfig` для доступа в Kubernetes через `kubectl`
```shell
microk8s config
```
3. Запуск Minio/Kafka/Kafka-REST/Zookeper/PostgreSQL/ClickHouse/ClickCat в кластере
```shell
kubectl apply -R -f 00_common/
```
4. **Опционально:** Добавление IP адреса кластера в файл `hosts` для доступа к приложениям через ingress
```shell
echo "$(microk8s config | grep server | grep -oE "\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}") minio.local clickhouse.local clickcat.local cp-kafka-rest.local clickhouse-app.local" >> /etc/hosts
```

## Примечания
* По умолчанию MicroK8S создаёт виртуальную машину с ОС Ubuntu 18.04LTS (cpu=2, memory=4G). При необходимости<br>
параметры ВМ можно увеличить через параметры команды `microk8s install`

## Ссылки
* [MicroK8S](https://microk8s.io/docs) - установка кластера Kubernetes для локальной разработки
* [Minikube](https://minikube.sigs.k8s.io/docs/) - установка кластера Kubernetes для локальной разработки
