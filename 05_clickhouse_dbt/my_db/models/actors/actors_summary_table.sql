{{ config(order_by='(updated_at, id, name)', engine='MergeTree()', materialized='table') }}

with actors_summary_table as (
SELECT id,
    any(actor_name) as name,
    uniqExact(movie_id)    as num_movies,
    avg(rank)                as avg_rank,
    uniqExact(genre)         as genres,
    uniqExact(director_name) as directors,
    max(created_at) as updated_at
FROM (
        SELECT {{ source('my_db', 'actors') }}.id as id,
                concat({{ source('my_db', 'actors') }}.first_name, ' ', {{ source('my_db', 'actors') }}.last_name) as actor_name,
                {{ source('my_db', 'movies') }}.id as movie_id,
                {{ source('my_db', 'movies') }}.rank as rank,
                genre,
                concat({{ source('my_db', 'directors') }}.first_name, ' ', {{ source('my_db', 'directors') }}.last_name) as director_name,
                created_at
        FROM {{ source('my_db', 'actors') }}
                    JOIN {{ source('my_db', 'roles') }} ON {{ source('my_db', 'roles') }}.actor_id = {{ source('my_db', 'actors') }}.id
                    LEFT OUTER JOIN {{ source('my_db', 'movies') }} ON {{ source('my_db', 'movies') }}.id = {{ source('my_db', 'roles') }}.movie_id
                    LEFT OUTER JOIN {{ source('my_db', 'genres') }} ON {{ source('my_db', 'genres') }}.movie_id = {{ source('my_db', 'movies') }}.id
                    LEFT OUTER JOIN {{ source('my_db', 'movie_directors') }} ON {{ source('my_db', 'movie_directors') }}.movie_id = {{ source('my_db', 'movies') }}.id
                    LEFT OUTER JOIN {{ source('my_db', 'directors') }} ON {{ source('my_db', 'directors') }}.id = {{ source('my_db', 'movie_directors') }}.director_id
        )
GROUP BY id
)

select *
from actors_summary_table