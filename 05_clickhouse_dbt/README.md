# Интеграция микросервиса с ClickHouse

## Сценарий
1. Подключение к ClickHouse через ClickCat
2. Загрузка данных из S3 хранилища в таблицы ClickHouse
3. Работа с DBT

## Подключение к ClickHouse через ClickCat
1. Открыть ClickCat UI через браузер
2. Создать подключение к ClickHouse (логин/пароль: `default`/`password`)
3. Создать базу данных `my_db` в ClickHouse
```clickhouse
create database if not exists
    my_db;
```

## Загрузка данных из S3 хранилища в таблицы ClickHouse
1. Создать таблицы в ClickHouse
```clickhouse
create table my_db.actors
(
    id         UInt32,
    first_name String,
    last_name  String,
    gender     FixedString(1)
) engine = MergeTree order by (id, first_name, last_name, gender);

create table my_db.directors
(
    id         UInt32,
    first_name String,
    last_name  String
) engine = MergeTree order by (id, first_name, last_name);

create table my_db.genres
(
    movie_id UInt32,
    genre    String
) engine = MergeTree order by (movie_id, genre);

create table my_db.movie_directors
(
    director_id UInt32,
    movie_id    UInt64
) engine = MergeTree order by (director_id, movie_id);

create table my_db.movies
(
    id   UInt32,
    name String,
    year UInt32,
    rank Float32 DEFAULT 0
) engine = MergeTree order by (id, name, year);

create table my_db.roles
(
    created_at DateTime DEFAULT now(),
    actor_id   UInt32,
    movie_id   UInt32,
    role       String
) engine = MergeTree order by (actor_id, movie_id);
```
2. Загрузить данные из S3 хранилища в ClickHouse
```clickhouse
insert into my_db.actors
select *
from s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_actors.tsv.gz',
        'TSVWithNames');

insert into my_db.directors
select *
from s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_directors.tsv.gz',
        'TSVWithNames');

insert into my_db.genres
select *
from s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_movies_genres.tsv.gz',
        'TSVWithNames');

insert into my_db.movie_directors
select *
from s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_movies_directors.tsv.gz',
        'TSVWithNames');

insert into my_db.movies
select *
from s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_movies.tsv.gz',
        'TSVWithNames');

insert into my_db.roles
select *
from s3('https://datasets-documentation.s3.eu-west-3.amazonaws.com/imdb/imdb_ijs_roles.tsv.gz',
        'TSVWithNames');
```

## Работа с DBT
1. Установка DBT
```shell
pip install -r 05_clickhouse_dbt/my_db/requirements.txt
```
2. Запуска DBT для материализации модели в ClickHouse ()
```shell
dbt run --project-dir my_db
```
3. Просмотр данных в Clickhouse
```clickhouse
--- Материализованная таблица с данными по модели
select
    * 
from
    my_db.actors_summary_table 
limit 10;

--- Материализованное представление с данными по модели
select
    *
from
    my_db.actors_summary_view
limit 10;
```
4. Автогенерация документации
```shell
dbt docs generate --project-dir my_db
```
5. Запуск web-сервера документации DBT
```shell
dbt docs serve --project-dir my_db
```

### Примечания
* Подробнее о типах материализации в DBT [DBT-Materializations](https://docs.getdbt.com/docs/build/materializations)
