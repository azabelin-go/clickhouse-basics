CREATE TABLE IF NOT EXISTS
    event(
        id Int,
        datetime DateTime,
        uuid String)
engine = MergeTree
order by id;
