package main

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"time"

	"github.com/ClickHouse/clickhouse-go/v2"
	"github.com/ClickHouse/clickhouse-go/v2/lib/driver"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"jsnow19890828/clickhouse-app/config"
)

const EventsTable = "event"

type clickhouseRepository struct {
	conn driver.Conn
}

func newClickhouseRepository(conn driver.Conn) *clickhouseRepository {
	return &clickhouseRepository{conn: conn}
}

func (cr *clickhouseRepository) rootHandler(ctx *gin.Context) {
	batch, err := cr.conn.PrepareBatch(ctx, fmt.Sprintf("insert into %s", EventsTable))
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	for i := 0; i < 100; i++ {
		err := batch.Append(
			int32(rand.Intn(100)),
			time.Now().UTC(),
			uuid.New().String(),
		)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
				"error": err.Error(),
			})
			return
		}
	}

	err = batch.Send()
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	ctx.JSON(http.StatusOK, gin.H{
		"result": "ok",
	})
}

func main() {
	fmt.Println()
	appCfg, err := config.Read()
	if err != nil {
		panic(fmt.Sprintf("failed to read app config: %s", err.Error()))
	}

	chConn, err := clickhouse.Open(&clickhouse.Options{
		Addr: []string{fmt.Sprintf("%s:%s", appCfg.Clickhouse.Host, appCfg.Clickhouse.Port)},
		Auth: clickhouse.Auth{
			Username: appCfg.Clickhouse.User,
			Password: appCfg.Clickhouse.Password,
			Database: appCfg.Clickhouse.Database,
		},
	})
	if err != nil {
		panic(fmt.Sprintf("failed to initialize clickhouse conenction: %s", err.Error()))
	}

	err = chConn.Ping(context.Background())
	if err != nil {
		panic(fmt.Sprintf("failed to connect to clickhouse: %s", err.Error()))
	}

	chRepo := newClickhouseRepository(chConn)

	router := gin.Default()
	router.GET("/", chRepo.rootHandler)

	err = router.Run(fmt.Sprintf(":%s", appCfg.App.ApiPort))
	if err != nil {
		panic(fmt.Sprintf("failed to to start server: %s", err.Error()))
	}
}
