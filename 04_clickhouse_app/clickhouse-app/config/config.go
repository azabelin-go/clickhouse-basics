package config

import (
	"sync"

	"github.com/ilyakaznacheev/cleanenv"
)

type (
	Config struct {
		App
		Clickhouse
	}

	App struct {
		ApiPort string `env:"APP_API_PORT" env-default:"8080"`
	}

	Clickhouse struct {
		User     string `env:"APP_CLICKHOUSE_USER" env-default:"default"`
		Password string `env:"APP_CLICKHOUSE_PASSWORD" env-default:"password"`
		Host     string `env:"APP_CLICKHOUSE_HOST" env-default:"localhost"`
		Port     string `env:"APP_CLICKHOUSE_PORT" env-default:"9000"`
		Database string `env:"APP_CLICKHOUSE_DB" env-default:"my_db"`
	}
)

var configInstance *Config
var configErr error

func Read() (*Config, error) {
	if configInstance == nil {
		var readConfigOnce sync.Once

		readConfigOnce.Do(func() {
			configInstance = &Config{}
			configErr = cleanenv.ReadEnv(configInstance)
		})
	}

	return configInstance, configErr
}
