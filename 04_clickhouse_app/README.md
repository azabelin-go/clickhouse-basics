# Интеграция микросервиса с ClickHouse

## Сценарий
1. Подключение к ClickHouse через ClickCat
2. Запуск микросервиса в Kubernetes
3. Запись данных через микросервис в ClickHouse

## Подключение к ClickHouse через ClickCat
1. Открыть ClickCat UI через браузер
2. Создать подключение к ClickHouse (логин/пароль: `default`/`password`)
3. Создать базу данных `my_db` в ClickHouse
```clickhouse
create database if not exists
    my_db;
```

## Запуск микросервиса в Kubernetes
1. Создать Namespace в Kubernetes для приложения
```shell
kubectl apply -f 04_clickhouse_app/k8s
```
2. Запустить миграции для создания таблицы в Clickhouse
```shell
kubectl apply -f 04_clickhouse_app/ch-migrations/k8s
```
3. Запуск микросервиса в Kubernetes
```shell
kubectl apply -f 04_clickhouse_app/clickhouse-app/k8s
```

## Запись данных через микросервис в ClickHouse
1. Запустить перенаправление портов в Kubernetes к микросервису:
```shell
kubectl -n clickhouse-app port-forward svc/clickhouse-app 8080:8080
```
2. Запись данных через микросервис в ClickHouse
```shell
curl http://localhost:8080/
```
3. Просмотр данных в Clickhouse
```clickhouse
select
    id,
    datetime,
    uuid
from
    my_db.events;
```

### Примечания
* Читать и публиковать сообщения в топики Kafka так же можно через ingress для микросервиса
```shell
curl http://clickhouse-app.local/
```
