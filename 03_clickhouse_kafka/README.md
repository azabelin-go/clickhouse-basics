# Интеграция ClickHouse с S3 сервисом

## Сценарий
1. Подготовка Kafka топиков
2. Подключение к ClickHouse через ClickCat
3. Работа с Kafka таблицами в ClickHouse

## Подготовка Kafka топиков
1. Создать топик в Kafka для чтения на сообщений
```shell
kubectl -n cp-kafka exec \
  -it pod/cp-kafka-0 \
  -- kafka-topics \
  --bootstrap-server localhost:9092 --create --topic clickhouse-in \
  --replication-factor 1 --partitions 6
```
2. Создать топик в Kafka для публикации сообщений
```shell
kubectl -n cp-kafka exec \
  -it pod/cp-kafka-0 \
  -- kafka-topics \
  --bootstrap-server localhost:9092 --create --topic clickhouse-out \
  --replication-factor 1 --partitions 6
```
3. Просмотр созданных топиков (опционально, необходимо для публикации/чтения сообщений через Kafka-REST)
```shell
kubectl -n cp-kafka exec \
  -it pod/cp-kafka-0 \
  -- kafka-topics --bootstrap-server localhost:9092 --list
```

## Подключение к ClickHouse через ClickCat
1. Открыть ClickCat UI через браузер
2. Создать подключение к ClickHouse (логин/пароль: `default`/`password`)
3. Создать базу данных `my_db` в ClickHouse
```clickhouse
create database if not exists
    my_db;
```

## Работа с Kafka таблицами в ClickHouse

### Чтение сообщений из Kafka очереди в таблицу ClickHouse
1. Создать таблицу на движке **MergeTree** для хранения сообщений
```clickhouse
create table
    my_db.kafka_in_table(
        id Int,
        data String)
engine = MergeTree
order by id;
```
2. Создать таблицу на движке Kafka для чтения сообщений
```clickhouse
create table
    my_db.kafka_in_queue(
        id Int,
        data String)
engine = Kafka()
settings
    kafka_broker_list = 'cp-kafka-headless.cp-kafka.svc.cluster.local:9092',
    kafka_topic_list = 'clickhouse-in',
    kafka_group_name = 'ch_cgroup_1',
    kafka_format = 'CSV',
    kafka_max_block_size = 1048576;
```
3. Создать материализованное представление для сохранения сообщений в таблицу
```clickhouse
create materialized view
    my_db.kafka_in_m_view
to
    my_db.kafka_in_table
as
    select
        id,
        data
    from
        my_db.kafka_in_queue;
```

4. Публикация сообщений в топик
```shell
kubectl -n cp-kafka exec \
  -it pod/cp-kafka-0 \
  -- sh -c \
  "echo '1,msg_1' | kafka-console-producer --bootstrap-server localhost:9092 --topic clickhouse-in"
```

5. Просмотр прочитанных сообщений
```clickhouse
select
    id,
    data
from
    my_db.kafka_in_table;
```

### Публикация сообщений в Kafka очередь через таблицу ClickHouse
1. Создать таблицу на движке Kafka для публикации сообщений
```clickhouse
create table
    my_db.kafka_out_queue(
        id Int,
        data String)
engine = Kafka()
    settings
        kafka_broker_list = 'cp-kafka-headless.cp-kafka.svc.cluster.local:9092',
        kafka_topic_list = 'clickhouse-out',
        kafka_group_name = 'ch_cgroup_1',
        kafka_format = 'CSV',
        kafka_max_block_size = 1048576;
```
2. Создать материализованное представление для сохранения сообщений в таблицу для публикации сообщений в Kafka
```clickhouse
create materialized view
    my_db.kafka_out_m_view
to
    my_db.kafka_out_queue
as
    select
        id,
        concat('transformed_', kit.`data`) as data
    from
        my_db.kafka_in_table kit;
```
3. Чтение сообщений из топика
```shell
kubectl -n cp-kafka exec \
  -it pod/cp-kafka-0 \
  -- sh -c \
  "kafka-console-consumer --bootstrap-server localhost:9092 --topic clickhouse-out --from-beginning"
```
4. Публикация сообщений в топик
```shell
kubectl -n cp-kafka exec \
  -it pod/cp-kafka-0 \
  -- sh -c \
  "echo '2,msg_2' | kafka-console-producer --bootstrap-server localhost:9092 --topic clickhouse-in"
```

### Примечания
* Читать и публиковать сообщения в топики Kafka так же можно и через приложение Kafka-REST\
Ссылка на документацию [REST Proxy API Reference](https://docs.confluent.io/platform/current/kafka-rest/api.html)
